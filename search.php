<?php
include 'header.php';
?>


   <div class="product-big-title-area">
       <div class="container">
           <div class="row">
               <div class="col-md-12">
                   <div class="product-bit-title text-center">
                       <h2>Search Results</h2><h4>You searched for: <?php echo $_POST['search']; ?> </h4>
                   </div>
               </div>
           </div>
       </div>
   </div>


   <div class="maincontent-area">
       <div class="container">
           <div class="row">
               <div class="col-md-12">
                   <div class="latest-product">
                       <div class="product-carousel">
                         <?php

                         if (isset($_POST['submit'])) {
                            $search = $_POST['search'];

                           $query = "SELECT * FROM products WHERE tags LIKE '%$search%'";
                           $search_query = mysqli_query ($connection, $query);
                           if(!$search_query){
                             die("QUERY FAILED" . mysqli_error($connection));
                           }
                           $count = mysqli_num_rows($search_query);
                           if ($count == 0) {
                             echo "<h1> NO RESULT </h1>";
                           } else {

                                 while($row = mysqli_fetch_assoc($search_query)){
                                   $product_id = $row['product_id'];
                                   $postdate = $row['post_date'];
                                   $Brand = $row['brand'];
                                   $Model = $row['model'];
                                   $Processor = $row['processor'];
                                   $Ram = $row['Ram'];
                                   $SSD = $row['ssd'];
                                   $HDD = $row['hdd'];
                                   $Display = $row['display'];
                                   $Ports = $row['ports'];
                                   $Tags = $row['tags'];
                                   $Recommended = $row['recommended'];
                                   $Image1 = $row['image1'];
                                   $Price = $row['price'];
                                   $Description = $row['Description'];
                                   $Category = $row['category'];
                                   $Quantity = $row['quantity'];
                                   ?>
                           <div class="single-product">
                               <div class="product-f-image">
                                   <img  src="img/<?php echo "$Image1"; ?>" width="" alt="">
                                   <div class="product-hover">
                                       <?php echo "<a href='productdetails.php?p_id={$product_id}' class='view-details-link'><i class=fa fa-link'></i> See details</a>" ?>
                                   </div>
                               </div>

                               <h2><?php echo "$Brand"; ?> <?php echo "$Model"; ?></h2>

                               <div class="product-carousel-price">
                                   <ins>KSH <?php echo "$Price"; ?>.00</ins>
                               </div>
                           </div>
                         <?php } ?>
                         <?php } ?>
                         <?php }
                        ?>

                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>



   <?php
   include 'footer.php';
    ?>
