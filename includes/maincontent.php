<div class="maincontent-area">
    <div class="zigzag-bottom"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="latest-product">
                    <h2 class="section-title">Latest Products</h2>
                    <?php


                    $query = "SELECT * FROM products ORDER BY post_date DESC LIMIT 10";
                    $display_laptops_query = mysqli_query($connection, $query);
                    if(!$display_laptops_query){
                      die("QUERY FAILED" . mysqli_error($connection));
                    }else {
                      $count = mysqli_num_rows($display_laptops_query);
                      if ($count == 0) {
                        echo "<h1> NO RESULT </h1>";
                      } else {

                        ?>


                    <div class="product-carousel">
                      <?php
                      while($row = mysqli_fetch_assoc($display_laptops_query)){
                        $product_id = $row['product_id'];
                        $postdate = $row['post_date'];
                        $Brand = $row['brand'];
                        $Model = $row['model'];
                        $Processor = $row['processor'];
                        $Ram = $row['Ram'];
                        $SSD = $row['ssd'];
                        $HDD = $row['hdd'];
                        $Display = $row['display'];
                        $Ports = $row['ports'];
                        $Tags = $row['tags'];
                        $Recommended = $row['recommended'];
                        $Image1 = $row['image1'];
                        $Price = $row['price'];
                        $Description = $row['Description'];
                        $Category = $row['category'];
                        $Quantity = $row['quantity'];
                        ?>
                        <div class="single-product">
                            <div class="product-f-image">
                                <img src="img/<?php echo "$Image1"; ?>" alt="">
                                <div class="product-hover">
                                    <a href="productdetails.php?p_id=<?php echo "$product_id"; ?>" class="view-details-link"><i class="fa fa-link"></i> See details</a>
                                </div>
                            </div>

                            <h2><a href="productdetails.php?p_id=<?php echo "$product_id"; ?>">  <?php echo "$Brand"; ?> <?php echo "$Model"; ?></a></h2>

                            <div class="product-carousel-price">
                                <ins>KSH <?php echo "$Price"; ?> /=</ins>
                            </div>
                        </div>
                      <?php }
                    }
                  } ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
