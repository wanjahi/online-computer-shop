<?php
include 'header.php';
?>
<style>
    #exampleInputName{
        height: 35px;
        width: 99%;
    }
    .tcal{

    }
</style>
<body>
    <?php
    if (isset($_GET['id'])) {
      $orderID = $_GET['id'];
      $query = "SELECT * FROM orders WHERE order_id = $orderID";
      $select_orders =mysqli_query($connection,$query);
      if (!$select_orders) {
        die("QUERY FAILED" .mysqli_error($connection));
      }else {
      while($row = mysqli_fetch_assoc($select_orders)){
        $email = $row['Email'];
        $total = $row['amount_payable'];
      }
    }
        ?>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span2">
                </div><!--/span-->
                <div class="span10">
                    <div style="margin-top: 30px; margin-bottom: 21px;">
                        <button  style="float:right;" class="btn btn-success btn-mini"><a href="javascript:Clickheretoprint()"> Print</button></a>
                    </div>
                    <div class="w3-row" style="width: 50%;margin-left: 15%">
                        <div class="content" id="content">
                            <div style="font-weight:bold; text-align:center;font-size:14px;margin-bottom: 15px;">
                                INVOICE RECEIPT NO: MSL-00<?php echo $orderID; ?>
                            </div>
                            <div style="font-weight:bold; text-align:left;font-size:14px;margin-bottom: 15px;">
                                MarkSonic Systems Limited. <br>
                                Head Office: Princely House, Moi Avenue, Nairobi. <br>
                                Mobile: 0712973246. <br>
                            </div>
                            <div style="font-weight:bold; text-align:left;font-size:14px;margin-bottom: 15px;">
                                Billing/Customer Details <br><br>
                                <?php
                                $query = "SELECT * FROM users WHERE Email = '{$email}'";
                               $select_users =mysqli_query($connection,$query);
                               if(!$select_users){
                                 die("QUERY FAILED" . mysqli_error($connection));
                               }else {
                               while($row = mysqli_fetch_assoc($select_users)){
                                 $db_Fname = $row['Fname'];
                                 $db_Lname = $row['Lname'];
                                 $db_Mobile = $row['Mobile'];
                                 $db_Email = $row['Email'];
                                 $db_County = $row['County'];
                                 $db_Town = $row['Town'];
                                 $db_Street = $row['Street'];
                                 $db_Building = $row['Building'];
                                 ?>
                                Name: <?php echo $db_Fname; ?><?php echo $db_Lname; ?> <br>
                                Email: <?php echo $db_Email; ?> <br>
                                Mobile: <?php echo $db_Mobile; ?><br>
                                County: <?php echo $db_County; ?> <br>
                                Town: <?php echo $db_Town; ?><br>
                                Street: <?php echo $db_Street; ?> <br>
                                Building: <?php echo $db_Building; ?> <br>
                              <?php }
                            } ?>
                            </div>
                            <div style="font-weight:bold; text-align:center;font-size:14px;margin-bottom: 15px;">
                                Order Items
                            </div>
                            <table class="table table-bordered" id="resultTable" data-responsive="table" style="text-align: left;">
                                <thead>
                                    <tr>
                                        <th width="13%"> Product Name </th>
                                        <th width="13%"> Operating System </th>
                                        <th width="13%"> Media Player </th>
                                        <th width="16%"> Text Editor </th>
                                        <th width="8%"> Quantity</th>
                                        <th width="18%"> Sub-Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php  $query = "SELECT * FROM order_items WHERE order_id = $orderID";
                                  $select_order_items =mysqli_query($connection,$query);
                                  while($row = mysqli_fetch_assoc($select_order_items)){
                                    $productID = $row['product_id'];
                                    $quantity = $row['quantity'];
                                    $os = $row['operating_system'];
                                    $media_player = $row['media_player'];
                                    $text_editor = $row['text_editor'];
                                    $query = "SELECT * FROM products WHERE product_id = $productID";
                                    $select_products_by_id = mysqli_query($connection,$query);
                                    while($row = mysqli_fetch_assoc($select_products_by_id)){

                                      $Brand = $row['brand'];
                                      $Model = $row['model'];
                                      $Price = $row['price'];

                                      $subTotal = $Price * $quantity;

                                          ?>
                                    <tr class="record">
                                      <td><?php echo $Brand; ?> <?php echo $Model; ?></td>
                                      <td><?php echo $os; ?></td>
                                      <td><?php echo $media_player; ?></td>
                                      <td><?php echo $text_editor; ?></td>
                                      <td><?php echo $quantity; ?></td>
                                      <td><?php echo $subTotal; ?></td>

                                      <?php
                      }
                      }
                                       ?>
                                    </tr>
                                </tbody>
                            </table>
                            <div style="font-weight:bold; text-align:center;font-size:14px;margin-bottom: 15px;">
                                Total Payable KSH. <?php echo number_format($total, 2); ?>
                            </div>
                            <div class="copyright">
                                <p>&copy; Marksonic Systems Limited. All Rights Reserved.</p>
                            </div>
                            <div class="col-md-4">
                              <p><img  height="" width="100" src="img/mpesa.jpeg" alt=""><div> PayBill No: 174379
                              </div></p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <?php
     }
    ?>
</body>
<script>
    function FAGga() {
        var loc = $("select[name='location']").val();
        //alert(loc);
        $("input[name='location']").val(loc);
    }
</script>
<script type="text/javascript" src="js/ourjs.js"></script>
<script type="text/javascript">
    window.onload(sTime());
</script>
<script language="javascript">
    function Clickheretoprint()
    {
        var disp_setting = "toolbar=yes,location=no,directories=yes,menubar=yes,";
        disp_setting += "scrollbars=yes,width=700, height=400, left=100, top=25";
        var content_vlue = document.getElementById("content").innerHTML;

        var docprint = window.open("", "", disp_setting);
        docprint.document.open();
        docprint.document.write('</head><body onLoad="self.print()" style="width: 700px; font-size:11px; font-family:arial; font-weight:normal;">');
        docprint.document.write(content_vlue);
        docprint.document.close();
        docprint.focus();
    }
</script>

<?php

include 'footer.php';
?>
