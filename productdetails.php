<?php
include 'header.php';


 ?>

    <div class="product-big-title-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-bit-title text-center">
                        <h2>Product Details</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="single-product-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="single-sidebar">
                        <h2 class="sidebar-title">Search Products</h2>
                        <form action="search.php" method="post">
                            <input type="text" placeholder="Search products...">
                            <input type="submit" value="Search">
                        </form>
                    </div>
                </div>

                <div class="col-md-8">

                        <div class="row">
                            <div class="col-sm-6">
                              <?php
                                            if (isset($_GET['p_id'])) {
                                              $product_id_details = $_GET['p_id'];
                                            $query = "SELECT * FROM products WHERE product_id = $product_id_details";
                                            $display_details_query = mysqli_query($connection, $query);
                                            if(!$display_details_query){
                                              die("QUERY FAILED" . mysqli_error($connection));
                                            }else{

                                                while($row = mysqli_fetch_assoc($display_details_query)){
                                                  $product_id = $row['product_id'];
                                                  $postdate = $row['post_date'];
                                                  $Brand = $row['brand'];
                                                  $Model = $row['model'];
                                                  $Processor = $row['processor'];
                                                  $Ram = $row['Ram'];
                                                  $SSD = $row['ssd'];
                                                  $HDD = $row['hdd'];
                                                  $Display = $row['display'];
                                                  $Ports = $row['ports'];
                                                  $Tags = $row['tags'];
                                                  $Recommended = $row['recommended'];
                                                  $Image1 = $row['image1'];
                                                  $Price = $row['price'];
                                                  $Description = $row['Description'];
                                                  $Category = $row['category'];
                                                  $sub_category = $row['sub_category'];
                                                  $Quantity = $row['quantity'];
                                                  $_SESSION['quantity'] = $Quantity;
                                                  ?>
                              <form method="post" action="cart.php?action=addToCart&&id=<?php echo $product_id; ?>" class="cart">
                                <div class="product-images">
                                    <div class="product-main-img">
                                        <img  src="img/<?php echo "$Image1"; ?>" width="" alt="">
                                        <input type="hidden" name="hidden_image" value="<?php echo "$Image1"; ?>">
                                    </div>


                                </div>
                            </div>

                            <div class="col-md-8">
                                <div class="product-inner">

                                    <h2 class="product-name"><?php echo "$Brand"; ?> <?php echo "$Model"; ?></h2>
                                    <div class="product-inner-price">
                                        <ins>KSH <?php echo "$Price"; ?>.00</ins>
                                    </div>
                                    <div role="tabpanel">
                                        <ul class="product-tab" role="tablist">
                                            <li role="presentation" class="active"><a href="" aria-controls="home" role="tab" data-toggle="tab">Description</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane fade in active" id="home">
                                                <h2>Product Description</h2>
                                                <p><?php echo "$Description"; ?></p>
                                                <h2>Recommended For: <?php echo "$Recommended"; ?></h2>
                                                <p></p>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="product-inner-category">
                                        <p>Category:  <?php echo "$Category"; ?> </p>
                                        <p>Sub-Category:  <?php echo "$sub_category"; ?> Computer </p>
                                          <p>Tags:   <?php echo "$Tags"; ?> </p>
                                    </div>
                                    <div class="input-text">
                                      <ul class="product-tab" role="tablist">
                                          <li role="presentation" class="active"><a href="" aria-controls="home" role="tab" data-toggle="tab">AfterSale Services Section</a></li>
                                      </ul>
                                      <p> <h3>Leave this fields empty if you are not familiar and we will install the most user-friendly softwares on your computer. Move to select your item quantity and proceed to add to shopping cart</h3> </p>
                                      <ul class="product-tab" role="tablist">
                                          <li role="presentation" class="active"><a href="" aria-controls="home" role="tab" data-toggle="tab">Operating System</a></li>
                                      </ul>

                                      <select class="form-control" size="" name="os" id="os" onChange="onSelectChange()">
                                         <option selected value="windows">Click To Choose your preferred Operating System</option>
                                         <option value="windows">Windows</option>
                                         <option value="linux">Linux</option>
                                       </select><br>

                                       <select class="form-control" size="1" name="windows" id="windows" style="display:none;">
                                         <?php


                                     $query = "SELECT * FROM windows_versions";
                                     $select_categories = mysqli_query($connection,$query);

                                     while($row = mysqli_fetch_assoc($select_categories)) {
                                     $version_id = $row['id'];
                                     $version_name = $row['name'];
                                     ?>
                                    <option value=" <?php echo $version_name; ?>  "><?php echo $version_name; ?></option>
                                  <?php } ?>
                                        </select><br>
                                        <select class="form-control" size="1" name="Linux" id="Linux" style="display:none;">
                                          <?php


                                      $query = "SELECT * FROM linux_versions";
                                      $select_categories = mysqli_query($connection,$query);

                                      while($row = mysqli_fetch_assoc($select_categories)) {
                                      $version_id = $row['id'];
                                      $version_name = $row['name'];
                                      ?>
                                     <option value=" <?php echo $version_name; ?>  "><?php echo $version_name; ?></option>
                                   <?php } ?>
                                         </select><br>

                                        <script>

                                        function onSelectChange() {
                                          var value = document.getElementById("os").value;
                                          if ( (value == 'windows')) {
                                            document.getElementById('windows').style.display = 'block';
                                            document.getElementById('Linux').style.display = 'none';
                                          }else if ((value == 'linux')) {
                                            document.getElementById('Linux').style.display = 'block';
                                            document.getElementById('windows').style.display = 'none';
                                          }else {
                                            document.getElementById('equipamento').style.display = 'none';
                                          }
                                        }
                                      </script>
                                      <ul class="product-tab" role="tablist">
                                          <li role="presentation" class="active"><a href="" aria-controls="home" role="tab" data-toggle="tab">Media Player</a></li>
                                      </ul>
                                      <select class="form-control" size="" name="media_player" id="media_player">
                                         <option selected value="vlc">Click To Choose your preferred Media Player</option>
                                         <?php


                                     $query = "SELECT * FROM media_players";
                                     $select_categories = mysqli_query($connection,$query);

                                     while($row = mysqli_fetch_assoc($select_categories)) {
                                     $version_id = $row['id'];
                                     $version_name = $row['name'];
                                     ?>
                                    <option value=" <?php echo $version_name; ?>  "><?php echo $version_name; ?></option>
                                  <?php } ?>
                                       </select><br>
                                      <ul class="product-tab" role="tablist">
                                          <li role="presentation" class="active"><a href="" aria-controls="home" role="tab" data-toggle="tab">Text Editor</a></li>
                                      </ul>
                                      <select class="form-control" size="" name="text_editor" id="area">
                                         <option selected value="notepadplus">Click To Choose your preferred Text Editors</option>
                                         <?php


                                     $query = "SELECT * FROM text_editors";
                                     $select_categories = mysqli_query($connection,$query);

                                     while($row = mysqli_fetch_assoc($select_categories)) {
                                     $version_id = $row['id'];
                                     $version_name = $row['name'];
                                     ?>
                                    <option value=" <?php echo $version_name; ?>  "><?php echo $version_name; ?></option>
                                  <?php } ?>
                                       </select><br>

                                    </div>







                                    <br>
                                      <ul class="product-tab" role="tablist">
                                          <li role="presentation" class="active"><a href="" aria-controls="home" role="tab" data-toggle="tab">In Stock: <?php echo $Quantity; ?> Pieces</a></li>
                                      </ul><br>
                                    <div class="quantity">
                                        <input type="number" size="5" class="input-text qty text" title="Qty" value="1" name="quantity" min="1" step="1">

                                    </div>
                                    <input type="hidden" name="hidden_name" value="<?php echo "$Brand"; ?> <?php echo "$Model"; ?>">
                                    <input type="hidden" name="hidden_price" value="<?php echo "$Price"; ?>">
                                    <button class="add_to_cart_button" type="submit" name="add">Add to cart</button>
                                  <?php } ?>
                                  <?php } ?>
                                  <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <?php
    include 'footer.php';
     ?>
