<?php
ob_start();
session_start();
include 'includes/functions.php';
include 'includes/db.php';

 ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Online Computer Store</title>

    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="admin/css/bootstrap.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="admin/font-awesome/css/font-awesome.min.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/responsive.css">
    <!-- Latest jQuery form server -->
    <script src="https://code.jquery.com/jquery.min.js"></script>
    <!-- <script type="text/javascript" src="">

    </script> -->

    <!-- Bootstrap JS form CDN -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

    <!-- jQuery sticky menu -->
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.sticky.js"></script>

    <!-- jQuery easing -->
    <script src="js/jquery.easing.1.3.min.js"></script>

    <!-- Main Script -->
    <script src="js/main.js"></script>



  </head>
  <body>

    <div class="header-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="user-menu">
                      <?php
                      if (empty($_SESSION['username'])) {
                        echo "<ul>
                            <li><a href='account.php?source=account'><i class='fa fa-user'></i> My Account</a></li>
                        </ul>";
                      } else {
                        ?>
                        <ul>
                          <?php echo "<li><a href='account.php?source=profile&&user_id={$_SESSION['username']}'><i class='fa fa-user'></i>
                          Profile
                          </a></li>
                          <li>
                                <a href='includes/logout.php'><i class='fa fa-power-off'></i> Log Out</a>
                            </li>
                            <li>
                              <a href='orderhistory.php?id={$_SESSION['username']}'>
                                <i class='fa fa-leaf'></i>Order History
                              </a>
                            </li>"; ?>
                            <?php
                            if($_SESSION['usergroup'] == 'admin'){


                              echo "<li><a href='admin'><i class='fa fa-gear'></i>
                              Admin Page
                              </a></li>";
                              }

                             ?>
                        </ul>
                    <?php  } ?>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="header-right">
                      <form action="search.php" method="post">
                      <div class="input-group">
                          <input name="search" type="text" class="form-control" placeholder="search by name, brand, specs etc..">
                          <span class="input-group-btn">
                              <button name="submit" class="btn btn-default" type="submit">
                                  <span class="glyphicon glyphicon-search"></span>
                          </button>
                          </span>
                      </div><!--search form-->
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End header area -->

    <div class="site-branding-area">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="logo">
                        <h1><a href="index.php">Marksonic <span>Systems</span></a></h1>
                    </div>
                </div>
                <div class="col-sm-6">
                  <?php if (isset($_SESSION['cart'])) {
                    $total = 0;

                    foreach ($_SESSION["cart"] as $key => $value) {
                      $total = $total + ($value["item_quantity"] * $value["product_price"]);}
                      ?>



                    <div class="shopping-item">
                        <a href="cart.php">Cart - <span class="cart-amunt">
                        <?php echo number_format($total, 2); ?>
                      </span> <i class="fa fa-shopping-cart"></i> <span class="product-count">
                       <?php
                        echo count($_SESSION['cart']);
                       ?>
                      </span></a>
                    </div>
                    <?php

                      } ?>
                </div>
            </div>
        </div>
    </div> <!-- End site branding area -->

    <div class="mainmenu-area">
        <div class="container">
            <div class="row">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="index.php">Home</a></li>
                        <?php

                        $query = "SELECT * FROM categories";
                        $select_categories = mysqli_query($connection,$query);

                        while($row = mysqli_fetch_assoc($select_categories)) {
                        $cat_id = $row['category_id'];
                        $cat_title = $row['category_title'];

              echo "<li><a href='categories.php?category={$cat_title}'>{$cat_title}s</a></li>";
          }

          ?>
                        <?php

                        $query = "SELECT * FROM sub_categories";
                        $select_categories = mysqli_query($connection,$query);

                        while($row = mysqli_fetch_assoc($select_categories)) {
                        $cat_id = $row['sub_category_id'];
                        $cat_title = $row['sub_category_title'];

              echo "<li><a href='subcategory.php?subcategory={$cat_title}'>{$cat_title}</a></li>";
          }

          ?>
                        <li><a href="contactus.php">Contact Us</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div> <!-- End mainmenu area -->

    <?php

     ?>
