<?php
include 'header.php';

if (isset($_POST["add"])) {
  if ($_POST["os"]=='windows') {
    $os = $_POST["windows"];
  }elseif ($_POST["os"]=='linux') {
    $os = $_POST["Linux"];
  }
  $stock = $_SESSION['quantity'];
  $select_quantity = $_POST['quantity'];
  if ($stock<$select_quantity) {
    $id =$_GET['id'];
    echo "<script>alert('You can not select more than the available product quantity. Please Select a quantity value less than or equal to the available product quantity and try again.');</script>";
    echo '<script>window.history.go(-1); </script>';
  } else {

  if (isset($_SESSION["cart"])) {
    $item_array_id = array_column($_SESSION["cart"], "product_id");
    if (!in_array($_GET["id"], $item_array_id)) {
      $count = count($_SESSION["cart"]);
      $item_array = array(
        'product_id' => $_GET["id"],
        'item_name'  => $_POST["hidden_name"],
        'os' => $os,
        'media_player' => $_POST["media_player"],
        'text_editor' => $_POST["text_editor"],
        'product_price' => $_POST["hidden_price"],
        'item_quantity' => $_POST["quantity"],
        'item_image' => $_POST["hidden_image"],
       );
       $_SESSION["cart"][$count] = $item_array;
       echo '<script>window.location="cart.php" </script>';

    }else {
      echo '<script>alert(Product is already added to cart) </script>';
      echo '<script>window.location="cart.php" </script>';

    }
  }else {
    $item_array = array(
      'product_id' => $_GET["id"],
      'item_name'  => $_POST["hidden_name"],
      'os' => $os,
      'media_player' => $_POST["media_player"],
      'text_editor' => $_POST["text_editor"],
      'product_price' => $_POST["hidden_price"],
      'item_quantity' => $_POST["quantity"],
      'item_image' => $_POST["hidden_image"],
     );
     $_SESSION["cart"][0] = $item_array;
  }
}
}
if (isset($_GET["action"])) {
  if ($_GET["action"] == "delete") {
    foreach ($_SESSION["cart"] as $keys => $value) {
      if ($value["product_id"] == $_GET["id"]) {
        unset($_SESSION["cart"][$keys]);
        echo '<script>alert(Product has been removed cart) </script>';
        echo '<script>window.location="cart.php" </script>';

      }
    }
  }
}

?>
<div class="product-big-title-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="product-bit-title text-center">
                    <h2>SHOPPING CART</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<div class="clear">

</div>
<div class="single-product-area">
    <div class="zigzag-bottom"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="product-content-right">
                    <div class="woocommerce">
                            <table cellspacing="0" class="shop_table cart">
                                <thead>
                                    <tr>
                                        <th class="product-remove">&nbsp;</th>
                                        <th class="product-thumbnail">&nbsp;</th>
                                        <th class="product-name">Product</th>
                                        <th class="product-price">Price</th>
                                        <th class="product-quantity">Quantity</th>
                                        <th class="product-subtotal">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php
                                    if (!empty($_SESSION["cart"])) {
                                      $total = 0;
                                      foreach ($_SESSION["cart"] as $key => $value) {
                                        ?>
                                        <tr class="cart_item">
                                            <td class="product-remove">
                                                <a title="Remove this item" class="remove" href="cart.php?action=delete&id=<?php echo $value["product_id"]; ?>">×</a>
                                            </td>

                                            <td class="product-thumbnail">
                                                <a href="single-product.html"><img width="145" height="145" alt="poster_1_up" class="shop_thumbnail" src="img/<?php echo $value["item_image"]; ?> "></a>
                                            </td>

                                            <td class="product-name">
                                                <a href="single-product.html"> <?php echo $value["item_name"]; ?>  </a>
                                            </td>

                                            <td class="product-price">
                                                <span class="amount"> <?php echo $value["product_price"]; ?> </span>
                                            </td>

                                            <td class="product-quantity">
                                                <?php echo $value["item_quantity"]; ?>
                                            </td>

                                            <td class="product-subtotal">

                                                <span class="amount"> <?php
                                                $item_subtotal = number_format($value["item_quantity"] * $value["product_price"], 2);

                                                echo $item_subtotal; ?> </span>
                                            </td>
                                        </tr>
                                        <tr>
                                          <?php
                                            $total = $total + ($value["item_quantity"] * $value["product_price"]);
                                            $_SESSION['pesa'] = $total;
                                      }
                                    }
                                    if (isset($_REQUEST['action']) && !empty($_REQUEST['action'])) {
                                      if ($_REQUEST['action']=='placeOrder') {
                                        if (!empty($_SESSION['username'])) {
                                        $username = $_SESSION['username'];
                                        $query = " INSERT INTO orders(order_date, Email, amount_payable, status) VALUES (now(), '{$username}', '{$total}', 'pending')";
                                        $orderQuery = mysqli_query($connection, $query);
                                        if(!$orderQuery){
                                          die("QUERY FAILED" . mysqli_error($connection));
                                        }else {
                                          $orderID = $connection->insert_id;
                                          foreach ($_SESSION["cart"] as $key => $value) {
                                           $query = " INSERT INTO order_items(order_id, product_id, operating_system, media_player, text_editor, quantity) VALUES ('{$orderID}', '{$value['product_id']}', '{$value['os']}', '{$value['media_player']}', '{$value['text_editor']}', '{$value['item_quantity']}')";
                                           $orderItemsQuery = mysqli_query($connection, $query);
                                           if(!$orderItemsQuery){
                                             die("QUERY FAILED" . mysqli_error($connection));
                                           }
                                          }
                                          header("Location: payment.php?id=$orderID");
                                        }
                                        }else {
                                          header("Location: account.php");
                                        }
                                        }
                                    }
                                 ?>
                                        <td class="actions" colspan="6">
                                            <a href="index.php">
                                              <input type="submit" value="CONTINUE SHOPPING" name="reset_cart" class="button">
                                            </a>

                                            <a href="checkout.php">
                                            <input type="submit" value="Proceed to Checkout" name="proceed" class="checkout-button button alt wc-forward">
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>


                            </div>
                            </div>
                            <div class="cart_totals ">
                            <h2>Cart Totals</h2>

                            <table cellspacing="0">
                                <tbody>
                                    <tr class="cart-subtotal">

                                        <th>Cart Subtotal</th>
                                        <td><span class="amount"><?php echo number_format($total, 2); ?></span></td>
                                    </tr>

                                    <tr class="shipping">
                                        <th>Shipping and Handling</th>
                                        <td>Free Shipping</td>
                                    </tr>

                                    <tr class="order-total">
                                        <th>Order Total</th>
                                        <td><strong><span class="amount"><?php echo number_format($total, 2); ?></span></strong> </td>
                                    </tr>
                                    </tbody>
                                  </table>
                                </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

      <?php

      include 'footer.php';
      ?>
