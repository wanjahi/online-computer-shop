<?php
include 'header.php';
 ?>

     <!-- slider -->
     <?php

include 'includes/slider.php';
      ?>

    <!-- End slider area -->

    <div class="promo-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="single-promo">
                        <i class="fa fa-refresh"></i>
                        <p>Warranty</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-promo">
                        <i class="fa fa-truck"></i>
                        <p>Free Deliveries</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-promo">
                        <i class="fa fa-lock"></i>
                        <p>Secure payments</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-promo">
                        <i class="fa fa-gift"></i>
                        <p>New products</p>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End promo area -->
      <!-- main content -->
     <?php
      include 'includes/maincontent.php';
      ?>

     <!-- End main content area -->

    <div class="brands-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="brand-wrapper">
                        <h2 class="section-title">Brands</h2>
                        <div class="brand-list">
                          <?php


                      $query = "SELECT * FROM brands";
                      $select_brands = mysqli_query($connection,$query);

                      while($row = mysqli_fetch_assoc($select_brands)) {
                      $brand_id = $row['id'];
                      $brand_name = $row['brand_name'];
                      $brand_image = $row['brand_image'];
                      ?>
                      <a href="brands.php?brand_name=<?php echo "$brand_name"; ?>"><img src="img/<?php echo "$brand_image"; ?>" alt=""></a>
                      <?php
                    }
                  ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End brands area -->

    <div class="product-widget-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                  <?php

                      $product_sub_category = 'High-end';
                      $query = "SELECT * FROM products WHERE sub_category = '{$product_sub_category}' ORDER BY product_id ASC LIMIT 3";
                      $display_laptops_query = mysqli_query($connection, $query);
                      if(!$display_laptops_query){
                        die("QUERY FAILED" . mysqli_error($connection));
                      }else {
                        $count = mysqli_num_rows($display_laptops_query);
                        if ($count == 0) {
                          echo "<h1> NO RESULT </h1>";
                        } else {

                          ?>
                    <div class="single-product-widget">
                        <h2 class="product-wid-title">High-End</h2>
                        <a href="subcategory.php?subcategory=High-end" class="wid-view-more">View All</a>
                        <?php
                        while($row = mysqli_fetch_assoc($display_laptops_query)){
                          $product_id = $row['product_id'];
                          $postdate = $row['post_date'];
                          $Brand = $row['brand'];
                          $Model = $row['model'];
                          $Processor = $row['processor'];
                          $Ram = $row['Ram'];
                          $SSD = $row['ssd'];
                          $HDD = $row['hdd'];
                          $Display = $row['display'];
                          $Ports = $row['ports'];
                          $Tags = $row['tags'];
                          $Recommended = $row['recommended'];
                          $Image1 = $row['image1'];
                          $Price = $row['price'];
                          $Description = $row['Description'];
                          $Category = $row['category'];
                          $Quantity = $row['quantity'];
                          ?>
                        <div class="single-wid-product">

                            <a href="productdetails.php"><img src="img/<?php echo "$Image1"; ?>" alt="" class="product-thumb"></a>
                            <h2><a href="productdetails.php?p_id=<?php echo "$product_id"; ?>"> <?php echo "$Brand"; ?> <?php echo "$Model"; ?></a></h2>
                            <div class="product-wid-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div>
                            <div class="product-wid-price">
                                <ins>KSH <?php echo "$Price"; ?> /=</ins>
                            </div>
                            </div>
                          <?php }
                        }
                      } ?>


                    </div>
                </div>
                <div class="col-md-4">
                  <?php

                  $product_sub_category = 'Budget';
                  $query = "SELECT * FROM products WHERE sub_category = '{$product_sub_category}' ORDER BY product_id ASC LIMIT 3";
                      $display_laptops_query = mysqli_query($connection, $query);
                      if(!$display_laptops_query){
                        die("QUERY FAILED" . mysqli_error($connection));
                      }else {
                        $count = mysqli_num_rows($display_laptops_query);
                        if ($count == 0) {
                          echo "<h1> NO RESULT </h1>";
                        } else {

                          ?>
                    <div class="single-product-widget">
                        <h2 class="product-wid-title">Budget</h2>
                        <a href="subcategory.php?subcategory=Budget" class="wid-view-more">View All</a>
                        <?php
                        while($row = mysqli_fetch_assoc($display_laptops_query)){
                          $product_id = $row['product_id'];
                          $postdate = $row['post_date'];
                          $Brand = $row['brand'];
                          $Model = $row['model'];
                          $Processor = $row['processor'];
                          $Ram = $row['Ram'];
                          $SSD = $row['ssd'];
                          $HDD = $row['hdd'];
                          $Display = $row['display'];
                          $Ports = $row['ports'];
                          $Tags = $row['tags'];
                          $Recommended = $row['recommended'];
                          $Image1 = $row['image1'];
                          $Price = $row['price'];
                          $Description = $row['Description'];
                          $Category = $row['category'];
                          $Quantity = $row['quantity'];
                          ?>
                        <div class="single-wid-product">


                            <a href="productdetails.php?p_id=<?php echo "$product_id"; ?>"><img src="img/<?php echo "$Image1"; ?>" alt="" class="product-thumb"></a>
                            <h2><a href="productdetails.php?p_id=<?php echo "$product_id"; ?>"> <?php echo "$Brand"; ?> <?php echo "$Model"; ?></a></h2>
                            <div class="product-wid-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div>
                            <div class="product-wid-price">
                                <ins>KSH <?php echo "$Price"; ?> /=</ins>
                            </div>

                        </div>
                      <?php }
                    }
                  } ?>

                    </div>
                </div>
                <div class="col-md-4">
                  <?php

                  $product_sub_category = 'Business';
                  $query = "SELECT * FROM products WHERE sub_category = '{$product_sub_category}' ORDER BY product_id ASC LIMIT 3";
                      $display_laptops_query = mysqli_query($connection, $query);
                      if(!$display_laptops_query){
                        die("QUERY FAILED" . mysqli_error($connection));
                      }else {
                        $count = mysqli_num_rows($display_laptops_query);
                        if ($count == 0) {
                          echo "<h1> NO RESULT </h1>";
                        } else {

                          ?>
                    <div class="single-product-widget">
                        <h2 class="product-wid-title">Business</h2>
                        <a href="subcategory.php?subcategory=Business" class="wid-view-more">View All</a>
                        <?php
                        while($row = mysqli_fetch_assoc($display_laptops_query)){
                          $product_id = $row['product_id'];
                          $postdate = $row['post_date'];
                          $Brand = $row['brand'];
                          $Model = $row['model'];
                          $Processor = $row['processor'];
                          $Ram = $row['Ram'];
                          $SSD = $row['ssd'];
                          $HDD = $row['hdd'];
                          $Display = $row['display'];
                          $Ports = $row['ports'];
                          $Tags = $row['tags'];
                          $Recommended = $row['recommended'];
                          $Image1 = $row['image1'];
                          $Price = $row['price'];
                          $Description = $row['Description'];
                          $Category = $row['category'];
                          $Quantity = $row['quantity'];
                          ?>




                          <div class="single-wid-product">


                              <a href="productdetails.php?p_id=<?php echo "$product_id"; ?>"><img src="img/<?php echo "$Image1"; ?>" alt="" class="product-thumb"></a>
                              <h2><a href="productdetails.php?p_id=<?php echo "$product_id"; ?>"> <?php echo "$Brand"; ?> <?php echo "$Model"; ?></a></h2>
                              <div class="product-wid-rating">
                                  <i class="fa fa-star"></i>
                                  <i class="fa fa-star"></i>
                                  <i class="fa fa-star"></i>
                                  <i class="fa fa-star"></i>
                                  <i class="fa fa-star"></i>
                              </div>
                              <div class="product-wid-price">
                                  <ins>KSH <?php echo "$Price"; ?> /=</ins>
                              </div>

                          </div>
                        <?php }
                      }
                    } ?>

                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End product widget area -->

    <?php
include 'footer.php';
     ?>
