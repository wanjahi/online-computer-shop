<?php
include 'header.php';




 ?>

    <div class="product-big-title-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-bit-title text-center">
                        <h2>CHECK-OUT</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="single-product-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">


                <div class="col-md-12">
                    <div class="product-content-right">
                        <div class="woocommerce">










                                <h3 id="order_review_heading">Your order</h3>

                                <div id="order_review" style="position: relative;">
                                    <table class="shop_table">
                                        <thead>
                                            <tr>
                                                <th class="product-name">Product</th>
                                                <th class="product-total">Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php
                                          if (!empty($_SESSION["cart"])) {
                                            $total = 0;
                                          foreach ($_SESSION["cart"] as $key => $value) {
                                            ?>
                                            <tr class="cart_item">
                                                <td class="product-name">
                                                    <?php echo $value["item_name"]; ?> <strong class="product-quantity">× <?php echo $value["item_quantity"]; ?></strong> </td>
                                                <td class="product-total">
                                                    <span class="amount"><?php
                                                    $item_subtotal = number_format($value["item_quantity"] * $value["product_price"], 2);

                                                    echo $item_subtotal; ?> </span> </td>
                                            </tr>
                                          <?php
                                          $total = $total + ($value["item_quantity"] * $value["product_price"]);

                                        }
                                      }?>
                                        </tbody>
                                        <tfoot>

                                            <tr class="cart-subtotal">
                                                <th>Cart Subtotal</th>
                                                <td><span class="amount"><?php echo number_format($total, 2); ?></span>
                                                </td>
                                            </tr>

                                            <tr class="shipping">
                                                <th>Shipping and Handling</th>
                                                <td>

                                                    Free Shipping
                                                    <input type="hidden" class="shipping_method" value="free_shipping" id="shipping_method_0" data-index="0" name="shipping_method[0]">
                                                </td>
                                            </tr>


                                            <tr class="order-total">
                                                <th>Order Total</th>
                                                <td><strong><span class="amount"><?php echo number_format($total, 2); ?></span></strong> </td>
                                            </tr>

                                        </tfoot>
                                    </table>
                                    <a href="cart.php?action=placeOrder">
                                    <input type="submit" value="PLACE ORDER" name="place_order" class="checkout-button button alt wc-forward">
                                    </a>
                                </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php

    include 'footer.php';
    ?>
