<div class="footer-top-area">
    <div class="zigzag-bottom"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="footer-about-us">
                    <h2>Marksonic <span>Systems</span></h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis sunt id doloribus vero quam laborum quas alias dolores blanditiis iusto consequatur, modi aliquid eveniet eligendi iure eaque ipsam iste, pariatur omnis sint! Suscipit, debitis, quisquam. Laborum commodi veritatis magni at?</p>
                    <div class="footer-social">
                        <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                        <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                        <a href="#" target="_blank"><i class="fa fa-youtube"></i></a>
                        <a href="#" target="_blank"><i class="fa fa-linkedin"></i></a>
                        <a href="#" target="_blank"><i class="fa fa-pinterest"></i></a>
                    </div>
                </div>
            </div>



            <div class="col-md-6 col-sm-6">
                <div class="footer-menu">
                    <h2 class="footer-wid-title">Categories</h2>
                    <ul>
                      <?php

                      $query = "SELECT * FROM categories";
                      $select_categories = mysqli_query($connection,$query);

                      while($row = mysqli_fetch_assoc($select_categories)) {
                      $cat_id = $row['category_id'];
                      $cat_title = $row['category_title'];

            echo "<li><a href='categories.php?category={$cat_title}'>{$cat_title}s</a></li>";
        }

        ?>
                      <?php

                      $query = "SELECT * FROM sub_categories";
                      $select_categories = mysqli_query($connection,$query);

                      while($row = mysqli_fetch_assoc($select_categories)) {
                      $cat_id = $row['sub_category_id'];
                      $cat_title = $row['sub_category_title'];

            echo "<li><a href='subcategory.php?subcategory={$cat_title}'>{$cat_title}</a></li>";
        }

        ?>
                    </ul>
                </div>
            </div>


        </div>
    </div>
</div> <!-- End footer top area -->

<div class="footer-bottom-area">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="copyright">
                    <p>&copy; Marksonic Systems Limited. All Rights Reserved.</p>
                </div>
            </div>

            <div class="col-md-4">
                <div class="footer-card-icon">
                    <i class="fa fa-cc-discover"></i>
                    <i class="fa fa-cc-mastercard"></i>
                    <i class="fa fa-cc-paypal"></i>
                    <i class="fa fa-cc-visa"></i>
                </div>
            </div>
        </div>
    </div>
</div> <!-- End footer bottom area -->


</body>
</html>
