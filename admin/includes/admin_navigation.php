
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.html">Admin</a>
    </div>
    <!-- Top Menu Items -->
    <ul class="nav navbar-right top-nav">
      <li>
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
                   <ul class="dropdown-menu message-dropdown">
                  <?php
                  $query = "SELECT * FROM contactus ORDER BY id DESC LIMIT 3 ";
                  $select_messages =
                  mysqli_query($connection,$query);
                  while($row = mysqli_fetch_assoc($select_messages)){
                    $message_id = $row['id'];
                    $date = $row['date'];
                    $email = $row['Email'];
                    $name = $row['Name'];
                    $Subject = $row['Subject'];
                    $message = $row['Message'];

                   ?>


                       <li class="message-preview">
                           <a href="./messages.php?source=message_details&id=<?php echo $row['id']; ?>">
                               <div class="media">
                                   <span class="pull-left">

                                   </span>
                                   <div class="media-body">
                                       <h5 class="media-heading">
                                           <strong><?php echo $name; ?></strong>
                                       </h5>
                                       <p class="small text-muted"><i class="fa fa-clock-o"></i> <?php echo $date; ?></p>
                                       <p><strong>Subject :</strong><?php echo $Subject; ?>
                                        <p><strong>Message :</strong> <?php echo $message; ?></p>
                                   </div>
                               </div>
                           </a>
                       </li>
                       <?php
                     } ?>


                       <li class="message-footer">
                           <a href="./messages.php">Read All New Messages</a>
                       </li>
                   </ul>
               </li>


      </li>
      <li><a href="../index.php">HOME SITE </a></li>




        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>  <?php
             echo $_SESSION['Fname'];?>
             <?php
              echo $_SESSION['Lname'];

              ?>
 <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li>
              <?php  echo "<a href='./profile.php?user_id={$_SESSION['username']}'><i class='fa fa-fw fa-user'></i> Profile</a>"; ?>
              </li>
              <li class="divider"></li>
              <li>
                    <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                </li>
            </ul>
        </li>
    </ul>





    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
            <li>
                <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
            </li>
            <li>
                <a href="javascript:;" data-toggle="collapse" data-target="#posts_dropdown"><i class="fa fa-fw fa-list"></i> PRODUCTS<i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="posts_dropdown" class="collapse">
                    <li>
                        <a href="products.php">View All Products</a>
                    </li>
                    <li>
                        <a href="products.php?source=add_product">Add Products</a>
                    </li>
                  </ul>
            </li>
            <li>
               <a href="javascript:;" data-toggle="collapse" data-target="#orders_dropdown"><i class="fa fa-fw fa-file"></i>ORDERS<i class="fa fa-fw fa-caret-down"></i></a>
               <ul id="orders_dropdown" class="collapse">
                   <li>
                       <a href="./orders.php"> View All Orders</a>
                   </li>
                   <li>
                       <a href="orders.php?source=pending_orders">Pending Orders</a>
                   </li>
               </ul>
               <li>
                  <a href="javascript:;" data-toggle="collapse" data-target="#messages_dropdown"><i class="fa fa-fw fa-envelope"></i> MESSAGES<i class="fa fa-fw fa-caret-down"></i></a>
                  <ul id="messages_dropdown" class="collapse">
                      <li>
                          <a href="./messages.php"> All Messages</a>
                      </li>
                      <li>
                          <a href="messages.php?source=new_messages">New Messages</a>
                      </li>
                  </ul>
           </li>
            <li>
                <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-user"></i> USERS <i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="demo" class="collapse">
                    <li>
                        <a href="users.php">View all users</a>
                    </li>
                    <li>
                        <a href="users.php?source=add_user">Add Users</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="categories.php"><i class="fa fa-fw fa-leaf"></i> CATEGORIES</a>
            </li>
            <li>
                <a href="subcategories.php"><i class="fa fa-fw fa-folder"></i> SUB-CATEGORIES</a>
            </li>
            <li>
                <a href="brands.php"><i class="fa fa-fw fa-eye"></i> BRANDS</a>
            </li>
            <li>
                <a href="windows_versions.php"><i class="fa fa-fw fa-magnet"></i> WINDOWS VERSIONS</a>
            </li>
            <li>
                <a href="linux_versions.php"><i class="fa fa-fw fa-magnet"></i> LINUX VERSIONS</a>
            </li>
            <li>
                <a href="text_editors.php"><i class="fa fa-fw fa-gears"></i> TEXT EDITORS</a>
            </li>
            <li>
                <a href="media_players.php"><i class="fa fa-fw fa-play"></i> MEDIA PLAYERS</a>
            </li>


        </ul>
    </div>




    <!-- /.navbar-collapse -->



</nav>
