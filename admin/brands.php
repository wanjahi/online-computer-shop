
    </div><!--Add Category Form--><?php include "includes/admin_header.php" ?>
<?php
?>
  <div id="wrapper">
        <!-- Navigation -->
        <?php include "includes/admin_navigation.php" ?>
        <?php
        if (isset($_POST['submit'])) {
          $brand_name = mysqli_real_escape_string($connection, $_POST['brand_name']);
          $brand_image = $_FILES['image']['name'];
          $brand_image_temp = $_FILES['image']['tmp_name'];
          move_uploaded_file($brand_image_temp, "./images/$brand_image");
          $query = "INSERT INTO brands(brand_name, brand_image)";
          $query .="VALUES ('{$brand_name}', '{$brand_image}')";
           $add_product_query= mysqli_query($connection, $query);
           echo "<script>alert('Brand Added successfully');</script>";

           if(!$add_product_query){
             die("QUERY FAILED" .mysqli_error($connection));
             echo "<script>alert('Failed to add new brand, try again');</script>";
           }
        }
         ?>
<div id="page-wrapper">

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <div class="col-xs-6">

            <?php  ?>

    <form action="" method="post" enctype="multipart/form-data">
      <div class="form-group">
         <label for="cat-title">Brand Name</label>
          <input type="text" class="form-control" name="brand_name">
      </div>
      <div class="form-group">
        <label for="image">Brand Image</label>
        <input type="file" name="image" class="form-control">
      </div>
       <div class="form-group">
          <input class="btn btn-primary" type="submit" name="submit" value="Add Brand">
      </div>

    </form>


    <div class="col-md-8">

</div>
<table width= '700' class="table table-bordered table-hover table-condensed">


        <thead>
            <tr>
                <th>Brand Id</th>
                <th>Brand Name</th>
                <th>Brand Image</th>
            </tr>
        </thead>
        <tbody>

        <?php


    $query = "SELECT * FROM brands";
    $select_brands = mysqli_query($connection,$query);

    while($row = mysqli_fetch_assoc($select_brands)) {
    $brand_id = $row['id'];
    $brand_name = $row['brand_name'];
    $brand_image = $row['brand_image'];

    echo "<tr>";

    echo "<td>{$brand_id}</td>";
    echo "<td>{$brand_name}</td>";
    ?>
     <td><img width="100" src="../img/<?php echo "$brand_image"; ?>" alt=""></td>
    <?php
    echo "<td><a href='brands.php?delete={$brand_id}'>Delete</a></td>";
    echo "</tr>";

    }




?>




        </tbody>
    </table>




                </div>


            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>



<?php

    if(isset($_GET['delete'])){
    $the_brand_id = $_GET['delete'];
    $query = "DELETE FROM brands WHERE id = {$the_brand_id} ";
    $delete_query = mysqli_query($connection,$query);
    header("Location: brands.php");


    }

 ?>





        <!-- /#page-wrapper -->

    <?php include "includes/admin_footer.php" ?>
