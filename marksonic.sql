-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 27, 2018 at 01:12 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `marksonic`
--

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(11) NOT NULL,
  `brand_name` text NOT NULL,
  `brand_image` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `brand_name`, `brand_image`) VALUES
(4, 'Dell', 'dell.png'),
(5, 'Apple', 'apple.png'),
(7, 'Acer', 'Acer.png'),
(8, 'Hp', 'hp.jpeg'),
(9, 'Sony', 'sony.png'),
(10, 'Samsung', 'Samsung.png'),
(11, 'Lenovo', 'lenovoproduct.jpg'),
(12, 'Asus', 'asus.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `callback`
--

CREATE TABLE `callback` (
  `id` int(100) NOT NULL,
  `merchantreqid` varchar(50) NOT NULL,
  `checkoutreqid` int(50) NOT NULL,
  `resultcode` varchar(50) NOT NULL,
  `ResultDesc` varchar(50) NOT NULL,
  `amount` int(20) NOT NULL,
  `mpesareceiptnumber` varchar(50) NOT NULL,
  `transtype` varchar(50) NOT NULL,
  `transactiondate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(100) NOT NULL,
  `category_title` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category_title`) VALUES
(1, 'Laptop'),
(2, 'Desktop');

-- --------------------------------------------------------

--
-- Table structure for table `contactus`
--

CREATE TABLE `contactus` (
  `id` int(100) NOT NULL,
  `date` date NOT NULL,
  `Email` varchar(30) NOT NULL,
  `Name` text NOT NULL,
  `Subject` varchar(20) NOT NULL,
  `Message` varchar(255) NOT NULL,
  `status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contactus`
--

INSERT INTO `contactus` (`id`, `date`, `Email`, `Name`, `Subject`, `Message`, `status`) VALUES
(1, '2018-05-14', 'kennedywanjahi@gmail.com', 'Kennedy Wanjahi', 'order', 'thanks for delivering', ''),
(2, '2018-05-17', 'Jacksongatuna@mail.com', 'Jackson Gatuna', 'Late Delivery', 'My Order Has not Been delivered its running late', ''),
(3, '2018-06-07', 'santanagitukia@gmail.com', 'Santana Gitukia ', 'order placement', 'I need you to place an order for me', ''),
(4, '2018-06-13', 'nancywandia@gmail.com', 'Nancy Wandia', 'Thank you Note', 'Thanks for delivering on time', ''),
(5, '2018-06-30', 'nancywandia@gmail.com', 'Nancy Wandia', 'Thank you Note', 'in time', ''),
(6, '2018-06-15', 'kennedywanjahi@gmail.com', 'kennedy wanjahi', 'Payment', 'are there more payment options', '');

-- --------------------------------------------------------

--
-- Table structure for table `linux_versions`
--

CREATE TABLE `linux_versions` (
  `id` int(10) NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `linux_versions`
--

INSERT INTO `linux_versions` (`id`, `name`) VALUES
(1, 'Ubuntu'),
(2, 'Elementary OS'),
(3, 'Fedora'),
(4, 'Kali Linux'),
(5, 'Manjaro Linux'),
(6, 'Arch Linux');

-- --------------------------------------------------------

--
-- Table structure for table `media_players`
--

CREATE TABLE `media_players` (
  `id` int(10) NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `media_players`
--

INSERT INTO `media_players` (`id`, `name`) VALUES
(1, 'VLC'),
(3, 'Banshee'),
(4, 'Roku');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(100) NOT NULL,
  `order_date` date NOT NULL,
  `Email` varchar(30) NOT NULL,
  `amount_payable` int(20) NOT NULL,
  `amount_paid` int(10) NOT NULL,
  `status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `order_date`, `Email`, `amount_payable`, `amount_paid`, `status`) VALUES
(6, '2018-05-14', 'denismuthoga@gmail.com', 200000, 0, 'completed'),
(12, '2018-05-14', 'denismuthoga@gmail.com', 98000, 0, 'completed'),
(17, '2018-06-02', 'derrickonallo@gmail.com', 98000, 0, 'completed'),
(19, '2018-06-15', 'www.leenick@gmail.com', 140000, 0, 'completed'),
(21, '2018-06-15', 'www.leenick@gmail.com', 340000, 0, 'completed'),
(24, '2018-06-18', 'kennedywanjahi@gmail.com', 140000, 0, 'pending'),
(25, '2018-06-18', 'kennedywanjahi@gmail.com', 33000, 0, 'pending'),
(26, '2018-06-18', 'kennedywanjahi@gmail.com', 300000, 0, 'pending'),
(27, '2018-06-18', 'kennedywanjahi@gmail.com', 140000, 0, 'pending'),
(28, '2018-06-18', 'kennedywanjahi@gmail.com', 440000, 0, 'pending'),
(34, '2018-06-19', 'Jasannwarui@gmail.com', 140000, 0, 'pending'),
(38, '2018-06-24', 'migos@migos.com', 420000, 0, 'pending'),
(39, '2018-06-24', 'kennedywanjahi@gmail.com', 420000, 0, 'pending'),
(40, '2018-06-24', 'kennedywanjahi@gmail.com', 98000, 0, 'pending'),
(41, '2018-06-24', 'kennedywanjahi@gmail.com', 98000, 0, 'pending'),
(42, '2018-06-25', 'kennedywanjahi@gmail.com', 140000, 0, 'pending'),
(43, '2018-06-25', 'kennedywanjahi@gmail.com', 238000, 0, 'pending'),
(44, '2018-06-25', 'kennedywanjahi@gmail.com', 45000, 0, 'pending'),
(45, '2018-06-25', 'migos@migos.com', 140000, 0, 'pending'),
(46, '2018-06-25', 'migos@migos.com', 45000, 0, 'pending'),
(47, '2018-06-25', 'migos@migos.com', 185000, 0, 'pending'),
(48, '2018-06-26', 'kennedywanjahi@gmail.com', 185000, 0, 'pending'),
(49, '2018-06-26', 'kennedywanjahi@gmail.com', 300000, 0, 'pending'),
(50, '2018-06-27', 'kennedywanjahi@gmail.com', 0, 0, 'pending'),
(51, '2018-06-27', 'kennedywanjahi@gmail.com', 0, 0, 'pending'),
(52, '2018-06-27', 'kennedywanjahi@gmail.com', 98000, 0, 'pending');

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` int(100) NOT NULL,
  `order_id` int(100) NOT NULL,
  `product_id` int(100) NOT NULL,
  `operating_system` varchar(30) NOT NULL,
  `media_player` text NOT NULL,
  `text_editor` text NOT NULL,
  `quantity` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `order_id`, `product_id`, `operating_system`, `media_player`, `text_editor`, `quantity`) VALUES
(1, 1, 0, '', '', '', 1),
(2, 2, 2, '', '', '', 1),
(3, 3, 1, '', '', '', 1),
(4, 4, 2, '', '', '', 1),
(5, 5, 2, '', '', '', 1),
(7, 7, 4, '', '', '', 1),
(8, 8, 3, '', '', '', 1),
(9, 9, 1, '', '', '', 1),
(10, 10, 3, '', '', '', 1),
(11, 11, 1, '', '', '', 1),
(12, 12, 2, '', '', '', 1),
(13, 13, 4, '', '', '', 1),
(14, 14, 2, '', '', '', 1),
(15, 15, 3, '', '', '', 1),
(16, 16, 2, '', '', '', 1),
(17, 17, 1, '', '', '', 1),
(18, 18, 4, '', '', '', 1),
(19, 16, 4, '', '', '', 1),
(20, 17, 2, '', '', '', 1),
(21, 18, 2, '', '', '', 1),
(22, 19, 3, '', '', '', 1),
(23, 20, 2, '', '', '', 1),
(24, 21, 3, '', '', '', 1),
(25, 21, 4, '', '', '', 1),
(26, 22, 3, '', '', '', 1),
(27, 22, 4, '', '', '', 1),
(28, 23, 2, '', '', '', 1),
(29, 24, 3, '', ' VLC  ', ' Sublime Text Editor  ', 1),
(30, 25, 7, '', ' Banshee  ', ' Brackets  ', 1),
(31, 26, 6, '', ' VLC  ', ' Brackets  ', 1),
(32, 27, 3, '', ' VLC  ', ' Notepad ++  ', 1),
(33, 28, 3, '', ' VLC  ', ' Notepad ++  ', 1),
(34, 28, 5, '', ' VLC  ', ' Brackets  ', 1),
(35, 29, 8, ' Elementary OS  ', ' VLC  ', ' Atom  ', 1),
(36, 30, 8, ' Elementary OS  ', ' VLC  ', ' Atom  ', 1),
(37, 31, 8, ' Elementary OS  ', ' VLC  ', ' Atom  ', 1),
(38, 31, 1, ' Windows 7  ', ' VLC  ', ' Atom  ', 1),
(39, 32, 3, ' Ubuntu  ', ' VLC  ', ' Atom  ', 1),
(40, 33, 4, ' Windows 7  ', 'vlc', 'notepadplus', 1),
(41, 34, 3, ' Manjaro Linux  ', ' Roku  ', ' Notepad ++  ', 1),
(42, 35, 2, ' Ubuntu  ', ' VLC  ', ' Atom  ', 1),
(43, 36, 2, ' Ubuntu  ', ' VLC  ', ' Atom  ', 1),
(44, 36, 5, ' Windows 10  ', ' Banshee  ', ' Atom  ', 1),
(45, 37, 2, ' Ubuntu  ', ' VLC  ', ' Atom  ', 1),
(46, 37, 5, ' Windows 10  ', ' Banshee  ', ' Atom  ', 1),
(47, 37, 6, ' Windows 7  ', ' VLC  ', ' Atom  ', 1),
(48, 38, 3, ' Windows 10  ', ' Banshee  ', ' Brackets  ', 3),
(49, 39, 3, ' Windows 10  ', ' Banshee  ', ' Brackets  ', 3),
(50, 40, 2, ' Windows 7  ', 'vlc', 'notepadplus', 1),
(51, 41, 2, ' Windows 7  ', 'vlc', 'notepadplus', 1),
(52, 42, 3, ' Windows 10  ', ' VLC  ', ' Atom  ', 1),
(53, 43, 3, ' Windows 10  ', ' VLC  ', ' Atom  ', 1),
(54, 43, 2, ' Ubuntu  ', ' VLC  ', ' Notepad ++  ', 1),
(55, 44, 1, ' Manjaro Linux  ', ' VLC  ', ' Atom  ', 1),
(56, 45, 3, ' Windows 10  ', ' Roku  ', ' Brackets  ', 1),
(57, 46, 1, ' Manjaro Linux  ', ' VLC  ', ' Atom  ', 1),
(58, 47, 3, ' Windows 10  ', ' Roku  ', ' Brackets  ', 1),
(59, 47, 1, ' Windows 7  ', 'vlc', 'notepadplus', 1),
(60, 48, 3, ' Windows 10  ', ' Roku  ', ' Brackets  ', 1),
(61, 48, 1, ' Windows 7  ', 'vlc', 'notepadplus', 1),
(62, 49, 6, ' Elementary OS  ', ' VLC  ', ' Brackets  ', 1),
(63, 52, 2, ' Windows 7  ', 'vlc', 'notepadplus', 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(100) NOT NULL,
  `post_date` date NOT NULL,
  `brand` text NOT NULL,
  `model` varchar(50) NOT NULL,
  `processor` varchar(50) NOT NULL,
  `Ram` varchar(50) NOT NULL,
  `ssd` varchar(50) DEFAULT NULL,
  `hdd` varchar(50) DEFAULT NULL,
  `display` varchar(255) NOT NULL,
  `ports` varchar(255) NOT NULL,
  `tags` varchar(255) NOT NULL,
  `recommended` varchar(100) NOT NULL,
  `image1` varchar(50) NOT NULL,
  `price` int(10) NOT NULL,
  `Description` varchar(255) NOT NULL,
  `category` varchar(50) NOT NULL,
  `sub_category` varchar(50) NOT NULL,
  `quantity` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `post_date`, `brand`, `model`, `processor`, `Ram`, `ssd`, `hdd`, `display`, `ports`, `tags`, `recommended`, `image1`, `price`, `Description`, `category`, `sub_category`, `quantity`) VALUES
(1, '2018-06-17', 'Hp', 'Folio 9470m', 'Intel Core i5-3427U (2x1.8GHz + HTT, Turbo to 2.8G', '1x4GB Hynix DDR3-1600', '180GB Intel 520 SATA 6Gbps SSD', '', '14', '2x USB 3.0, DisplayPort, SD/MMC Reader, VGA Docking port, Ethernet, AC adaptor Vent, USB 3.0 charging port, Mic/headphone combo jack, SmartCard reader', 'laptop, business, ultrabook', 'business, travelling persons, corporate business,', 'folio9470m.jpg', 45000, 'the Folio looks like a business laptop because it is one: an elegantly compact one at that. The two-tone magnesium (silver on the top, black underneath) has a soft-touch finish. Everything from the cleanly inset matte 14-inch screen to the crisp black key', 'Laptop', 'Business', 20),
(2, '2018-05-13', 'Hp', 'Spectre x360 - 13-4001ng', 'Intel Core i7-5500U with Intel HD Graphics 5500 (2', '8 GB 1600 MHz LPDDR3 SDRAM (onboard)', '256 GB M.2 SSD', '', '33.8 cm (13.3', '1 multi-format SD media card reader1 HDMI1 headphone/microphone combo3 USB 3.01 Mini DisplayPort', 'laptop, business, ultrabook', 'business, travelling persons, corporate business, ', 'spectre.jpg', 98000, 'From top to bottom, the new Spectre x360 is the most striking 2-in-1 HP has ever made. And by shaving excess metal off from almost every side of the machine, HP also made the x360 one of the most portable 2-in-1s available. This spring, the Spectre x360 g', 'Laptop', 'Business', 20),
(3, '2018-05-13', 'Lenovo', 'Legion Y520', '7th Generation Intel Core i7 processor', '16GB DDR4 memory', '512GB PCIe SSD', '2TB HDD storage', '15.6in FHD (1920 x 1080) anti-glare display with IPS technology', '2 USB 3.0, USB 2.0, HDMI, USB Type C, RJ45, 4-in-1 card reader, audio jack, microphone jack', 'laptop, gaming, ssd, perfomance, graphics card', 'gaming, video editing, rendering, programming and other resource intensive uses', 'lenovolegiony520.jpg', 140000, 'This slim, lightweight, portable gaming PC, should be taking over your gaming lifestyle soon. Powered by 7th Gen Intel Core processors and NVIDIA GTX 1050 discrete graphics, Lenovo doesnâ€™t just stop there. They also made sure to feature premium audio an', 'Laptop', 'High-end', 20),
(4, '2018-05-13', 'Apple', 'Macbook Air 13-inch', '1.8GHz dual-core Intel Core i5, Turbo Boost up to ', 'Macbook Air 13-inch', '128GB PCIe-based SSD', '', '13.3-inch (diagonal) LED-backlit glossy widescreen display with support for millions of colors Intel HD Graphics 6000', 'Two USB 3 ports (up to 5 Gbps)  Thunderbolt 2 port (up to 20 Gbps)  MagSafe 2 power port  SDXC card slot  3.5 mm headphone jack', 'laptop, business, ultrabook', 'business, travelling persons, corporate business, ', 'macbook.jpg', 200000, 'The 13-inch MacBook Air features 8GB of memory, a fifth-generation Intel Core processor, Thunderbolt 2, great built-in apps and all-day battery life. Itâ€™s thin, light and durable enough to take everywhere you go and powerful enough to do everything once', 'Laptop', 'Business', 20),
(5, '2018-06-15', 'Lenovo', 'Legion920', '', '', '', '', '', '', '', '', 'LenovoLegionY920.png', 300000, '', 'Desktop', 'High-end', 50),
(6, '2018-06-17', 'Dell', '7567 gaming', 'core i9', '32gb', '512gb', '2tb', '4k 32inch wide', 'vga, hdmi, rca', 'gaming, rendering, animation, high-end, ssd', 'gamers, resource intensive users', 'dellinspiron7567gaming.jpg', 300000, 'a high end gaming laptop', 'Laptop', 'High-end', 10),
(7, '2018-06-17', 'Dell', 'Inspiron 16', 'core i3', '4gb', 'n/a', '500gb', '14 inch', 'vga, hdmi,', 'budget', 'students', 'DellInspironbudget.png', 33000, 'meets the task', 'Laptop', 'Budget', 10),
(8, '2018-06-17', 'Lenovo', 'G50', 'core i3', '4gb', 'n/a', '500gb', '14inch 1366*768p', 'vga, hdmi,', 'budget', 'students', 'lenovo.jpg', 32000, 'meets most tasks', 'Laptop', 'Budget', 10),
(9, '2018-06-17', 'Samsung', 'bt10', 'pentium duo core', '4gb', 'n/a', '500gb', '17inch wide dispaly', 'vga,', 'desktop, budget', 'students', 'samsungdesktopbudget.jpg', 20000, 'small compact budget desktop', 'Desktop', 'Budget', 20);

-- --------------------------------------------------------

--
-- Table structure for table `sender_details`
--

CREATE TABLE `sender_details` (
  `id` int(100) NOT NULL,
  `businesscode` varchar(30) NOT NULL,
  `mpestimestamp` varchar(30) NOT NULL,
  `transactiontype` varchar(30) NOT NULL,
  `amount` int(20) NOT NULL,
  `sendernumber` varchar(15) NOT NULL,
  `paybill` int(15) NOT NULL,
  `accountref` varchar(50) NOT NULL,
  `transdec` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sender_details`
--

INSERT INTO `sender_details` (`id`, `businesscode`, `mpestimestamp`, `transactiontype`, `amount`, `sendernumber`, `paybill`, `accountref`, `transdec`) VALUES
(38, '174379', '20180622120046', 'CustomerPayBillOnline', 398000, '254', 174379, 'Marksonic Systems', 'marksonic'),
(39, '174379', '20180622120734', 'CustomerPayBillOnline', 698000, '254712973246', 174379, 'Marksonic Systems', 'marksonic'),
(40, '174379', '20180624121609', 'CustomerPayBillOnline', 420000, '+2547', 174379, 'Marksonic Systems', 'marksonic'),
(41, '174379', '20180624124908', 'CustomerPayBillOnline', 420000, '254712973246', 174379, 'Marksonic Systems', 'marksonic'),
(42, '174379', '20180624125001', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems', 'marksonic'),
(43, '174379', '20180624125240', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems', 'marksonic'),
(44, '174379', '20180624125604', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'MIKELTD', 'MAKAO'),
(45, '174379', '20180624125707', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'MIKELTD', 'MAKAO'),
(46, '174379', '20180624125757', 'CustomerPayBillOnline', 420000, '254712973246', 174379, 'MIKELTD', 'MAKAO'),
(47, '174379', '20180624010323', 'CustomerPayBillOnline', 420, '254712973246', 174379, 'MIKELTD', 'MAKAO'),
(48, '174379', '20180624010442', 'CustomerPayBillOnline', 420, '254712973246', 174379, 'MIKELTD', 'MAKAO'),
(49, '174379', '20180624010519', 'CustomerPayBillOnline', 420, '254712973246', 174379, 'MIKELTD', 'MAKAO'),
(50, '174379', '20180624010604', 'CustomerPayBillOnline', 420000, '254712973246', 174379, 'MIKELTD', 'MAKAO'),
(51, '174379', '20180624010612', 'CustomerPayBillOnline', 420000, '254712973246', 174379, 'MIKELTD', 'MAKAO'),
(52, '174379', '20180624010643', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'MIKELTD', 'MAKAO'),
(53, '174379', '20180624010709', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'MIKELTD', 'MAKAO'),
(54, '174379', '20180624010839', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'MIKELTD', 'MAKAO'),
(55, '174379', '20180624011548', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'MIKELTD', 'MAKAO'),
(56, '174379', '20180624011648', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'marksonic', 'Kennedy Wanjahi'),
(57, '174379', '20180624011729', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(58, '174379', '20180624011748', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(59, '174379', '20180624011816', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(60, '174379', '20180624011912', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(61, '174379', '20180624012016', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(62, '174379', '20180624012226', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(63, '174379', '20180624012313', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(64, '174379', '20180624015400', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(65, '174379', '20180624015512', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(66, '174379', '20180625044616', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(67, '174379', '20180625044656', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(68, '174379', '20180625044845', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(69, '174379', '20180625045200', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(70, '174379', '20180625045243', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(71, '174379', '20180625045439', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(72, '174379', '20180625045458', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(73, '174379', '20180625045518', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(74, '174379', '20180625045537', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(75, '174379', '20180625045802', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(76, '174379', '20180625050230', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(77, '174379', '20180625050502', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(78, '174379', '20180625050812', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(79, '174379', '20180625050834', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(80, '174379', '20180625051311', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(81, '174379', '20180625051451', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(82, '174379', '20180625052000', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(83, '174379', '20180625052018', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(84, '174379', '20180625052348', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(85, '174379', '20180625052713', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(86, '174379', '20180625052755', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(87, '174379', '20180625053206', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(88, '174379', '20180625053311', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(89, '174379', '20180625054137', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(90, '174379', '20180625054441', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(91, '174379', '20180625054507', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(92, '174379', '20180625054509', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(93, '174379', '20180625054510', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(94, '174379', '20180625054537', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(95, '174379', '20180625054629', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(96, '174379', '20180625054748', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(97, '174379', '20180625054948', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(98, '174379', '20180625055209', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(99, '174379', '20180625055304', 'CustomerPayBillOnline', 1000, '+2547', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(100, '174379', '20180625055340', 'CustomerPayBillOnline', 1000, '+2547', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(101, '174379', '20180625055649', 'CustomerPayBillOnline', 1000, '+2547', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(102, '174379', '20180625055722', 'CustomerPayBillOnline', 1000, '+2547', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(103, '174379', '20180625055808', 'CustomerPayBillOnline', 1000, '+2547', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(104, '174379', '20180625055844', 'CustomerPayBillOnline', 1000, '+2547', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(105, '174379', '20180625055923', 'CustomerPayBillOnline', 1000, '+2547', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(106, '174379', '20180625060003', 'CustomerPayBillOnline', 1000, '+2547', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(107, '174379', '20180625060128', 'CustomerPayBillOnline', 1000, '+2547', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(108, '174379', '20180625060325', 'CustomerPayBillOnline', 1000, '+2547', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(109, '174379', '20180625060716', 'CustomerPayBillOnline', 1000, '+2547', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(110, '174379', '20180625060805', 'CustomerPayBillOnline', 1000, '+2547', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(111, '174379', '20180625060826', 'CustomerPayBillOnline', 1000, '+2547', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(112, '174379', '20180625060857', 'CustomerPayBillOnline', 1000, '+2547', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(113, '174379', '20180625060945', 'CustomerPayBillOnline', 1000, '+2547', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(114, '174379', '20180625061451', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(115, '174379', '20180625062320', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(116, '174379', '20180625062422', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(117, '174379', '20180625062449', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(118, '174379', '20180625062505', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(119, '174379', '20180625062519', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(120, '174379', '20180625062622', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(121, '174379', '20180625062651', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(122, '174379', '20180625062716', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(123, '174379', '20180625062746', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(124, '174379', '20180625062816', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(125, '174379', '20180625063447', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(126, '174379', '20180625063948', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(127, '174379', '20180625063948', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(128, '174379', '20180625064427', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(129, '174379', '20180625070538', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(130, '174379', '20180626111411', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi'),
(131, '174379', '20180626121053', 'CustomerPayBillOnline', 1000, '254712973246', 174379, 'Marksonic Systems Limited', 'Kennedy Wanjahi');

-- --------------------------------------------------------

--
-- Table structure for table `sms`
--

CREATE TABLE `sms` (
  `id` int(100) NOT NULL,
  `messageId` varchar(50) NOT NULL,
  `message` varchar(255) NOT NULL,
  `status` varchar(20) NOT NULL,
  `sender` varchar(20) NOT NULL,
  `reciever` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sms`
--

INSERT INTO `sms` (`id`, `messageId`, `message`, `status`, `sender`, `reciever`) VALUES
(1, 'ATXid_c954b202c633be3adbd698f65971d7f4', 'Dear customer, you have successfully placed an order pay to paybill 174379', 'sent', 'MarksonicSystems', '+254704752496'),
(2, 'ATXid_331a2766d297bb384e3e093212c20ea9', 'Dear customer, you have successfully placed an order pay to paybill 174379', 'sent', 'MarksonicSystems', '+254712973246'),
(3, 'ATXid_937644f3cc8971a3fd3d27d7377dbc43', 'Dear customer, you have successfully placed an order pay to paybill 174379', 'sent', 'MarksonicSystems', '+254712973246'),
(4, 'ATXid_7c9457eebdfbcedec95149263d069d41', 'Dear customer, you have successfully placed an order pay to paybill 174379', 'sent', 'MarksonicSystems', '+254712973246'),
(5, 'ATXid_666ac6b163bc607622d58720943d332a', 'Dear customer, you have successfully placed an order pay to paybill 174379', 'sent', 'MarksonicSystems', '+254726422225'),
(6, 'ATXid_6af200804abb6c863acd78f942cb326d', 'Dear customer, you have successfully placed an order pay to paybill 174379', 'sent', 'MarksonicSystems', '+254726422225'),
(7, 'ATXid_eb64ee291390a36cf620fef2d235e296', 'Dear customer, you have successfully placed an order pay to paybill 174379', 'sent', 'MarksonicSystems', '+254726422225'),
(8, 'ATXid_3e9f7bc59aa66d212c9449bbe6c3789d', 'Dear customer, you have successfully placed an order pay to paybill 174379', 'sent', 'MarksonicSystems', '+254729707588'),
(9, 'ATXid_30b57ec1c56eac845491b6ee09e56168', 'Dear customer, you have successfully placed an order pay to paybill 174379', 'sent', 'MarksonicSystems', '+254712973246');

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `sub_category_id` int(2) NOT NULL,
  `sub_category_title` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`sub_category_id`, `sub_category_title`) VALUES
(1, 'High-end'),
(2, 'Business'),
(3, 'Budget');

-- --------------------------------------------------------

--
-- Table structure for table `text_editors`
--

CREATE TABLE `text_editors` (
  `id` int(10) NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `text_editors`
--

INSERT INTO `text_editors` (`id`, `name`) VALUES
(1, 'Atom'),
(2, 'Brackets'),
(4, 'Notepad ++'),
(7, 'Sublime Text Editor');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `Fname` text NOT NULL,
  `Lname` text NOT NULL,
  `Mobile` varchar(15) NOT NULL,
  `Email` varchar(30) NOT NULL,
  `usergroup` text NOT NULL,
  `County` text NOT NULL,
  `Town` text NOT NULL,
  `Street` varchar(20) NOT NULL,
  `Building` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`Fname`, `Lname`, `Mobile`, `Email`, `usergroup`, `County`, `Town`, `Street`, `Building`, `password`) VALUES
('customer', 'care', '+254712973246', 'customercare@marksonic.com', 'admin', 'Nairobi', 'Nairobi CBD', 'Moi Avenue', 'Princely House', '18e25e2f57412d751b395ba390d81544'),
('Denis', 'Muthoga', '+254704752496', 'denismuthoga@gmail.com', 'customer', 'Nairobi', 'kayole', 'spine road', 'aa plaza', '827ccb0eea8a706c4c34a16891f84e7b'),
('Derrick', 'Onallo', '+254725931131', 'derrickonallo@gmail.com', 'customer', 'Mombasa', 'Ukunda', '', '', '4dc00afde40a3a569516a1b4239b8cb8'),
('Jasany', 'Warui', '+254729707588', 'Jasannwarui@gmail.com', 'customer', 'Nairobi Province', 'Nairobi', 'Nairobi', 'Princely house', '0654e0e0366863043e5e0d3d6185db33'),
('Kennedy', 'Wanjahi', '+254712973246', 'kennedywanjahi@gmail.com', 'admin', 'Nairobi', 'Nairobi CBD', 'Moi Avenue', 'Bihi Towers 5th floor suite 10', '18e25e2f57412d751b395ba390d81544'),
('migos', 'migos', '+2547', 'migos@migos.com', 'customer', 'migos', 'migos', 'migos', 'migos', 'd4774a38c584f7d602aedb52b3e189b3'),
('Paul ', 'Muchiri ', '+254721763735', 'Wanjaumuchiri@yahoo.com', 'customer', 'Nairobi ', 'Nairobi ', 'Moi Avenue ', 'Princely house', '45b98f4ad72a267bf12f1ee6e0adb6e2'),
('Kevin', 'Njiru', '+254726422225', 'www.leenick@gmail.com', 'customer', 'Nairobi', 'Dandora', 'Phase 4', 'Charity', '365cc4c8271aacfb89255556821269c9');

-- --------------------------------------------------------

--
-- Table structure for table `windows_versions`
--

CREATE TABLE `windows_versions` (
  `id` int(10) NOT NULL,
  `name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `windows_versions`
--

INSERT INTO `windows_versions` (`id`, `name`) VALUES
(1, 'Windows 7'),
(2, 'Windows 8'),
(7, 'Windows 8.1'),
(8, 'Windows 10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `callback`
--
ALTER TABLE `callback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `contactus`
--
ALTER TABLE `contactus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `linux_versions`
--
ALTER TABLE `linux_versions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media_players`
--
ALTER TABLE `media_players`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `sender_details`
--
ALTER TABLE `sender_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sms`
--
ALTER TABLE `sms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`sub_category_id`);

--
-- Indexes for table `text_editors`
--
ALTER TABLE `text_editors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`Email`);

--
-- Indexes for table `windows_versions`
--
ALTER TABLE `windows_versions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `callback`
--
ALTER TABLE `callback`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `contactus`
--
ALTER TABLE `contactus`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `linux_versions`
--
ALTER TABLE `linux_versions`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `media_players`
--
ALTER TABLE `media_players`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `sender_details`
--
ALTER TABLE `sender_details`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- AUTO_INCREMENT for table `sms`
--
ALTER TABLE `sms`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `sub_category_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `text_editors`
--
ALTER TABLE `text_editors`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `windows_versions`
--
ALTER TABLE `windows_versions`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
